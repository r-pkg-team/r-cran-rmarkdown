Source: r-cran-rmarkdown
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-bslib,
               r-cran-evaluate,
               r-cran-fontawesome,
               r-cran-htmltools,
               r-cran-jquerylib,
               r-cran-knitr (>= 1.43),
               r-cran-tinytex (>= 0.31),
               r-cran-xfun (>= 0.36),
               r-cran-yaml,
               libjs-jquery
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-rmarkdown
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-rmarkdown.git
Homepage: https://cran.r-project.org/package=rmarkdown
Rules-Requires-Root: no

Package: r-cran-rmarkdown
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends},
         libjs-bootstrap,
         libjs-highlight.js,
         libjs-jquery-ui,
         libjs-modernizr,
         libjs-prettify,
         r-cran-jquerylib,
         fonts-font-awesome,
         pandoc,
         r-cran-shiny,
         node-html5shiv
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: convert R markdown documents into a variety of formats
 R Markdown is a framework for creating documents that mix R code with
 markdown to produce visually pleasing, high quality and reproducible
 reports. It supports various output formats, including HTML, PDF,
 Microsoft Word and Beamer.
 .
 Please note: Upstream rmarkdown contains export to ioslides.  This was
 removed from the Debian package due to not existing license statement.
 If you need this functionality you need to install rmarkdown manually
 from CRAN.
